import React, { Component } from "react";
import Label from "../Label";
import "./SingleIssue.css";
import { token } from "../Authentication";
var moment = require("moment");

class SingleIssue extends Component {
  state = {
    result: undefined,
    comments: undefined,
    issuesNumber: this.props.match.params.number
  };

  componentDidMount() {
    let IssuesNumber = this.props.match.params.number;
    fetch(
      `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${IssuesNumber}`
    )
      .then(res => res.json())
      .then(issuse =>
        this.setState({
          result: issuse
        })
      )
      .catch(err => console.log(err));

    fetch(
      `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${IssuesNumber}/comments`
    )
      .then(res => res.json())
      .then(commentsData =>
        this.setState({
          comments: commentsData
        })
      )
      .catch(err => console.log(err));
  }

  render() {
    console.log(token)
    console.log("render");
    if (this.state.result && this.state.comments) {
      const addComment = event => {
        if (token) {
          if (event.key === "Enter") {
            fetch(
              `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues/${
                this.state.issuesNumber
              }/comments?access_token=${token}`,
              {
                method: "post",
                headers: {
                  Accept: "application/json, text/plain, */*",
                  "Content-Type": "application/json"
                },
                body: JSON.stringify({ body: event.target.value })
              }
            )
              .then(res => res.json())
              .then(newComment => {
                console.log(newComment);
                let allComments = this.state.comments;
                allComments.push(newComment);
                this.setState({
                  comments: allComments
                });
              });
          }
        } else {
          alert("Sign In first!!..");
        }
      };
      console.log(this.state.comments)

      let id = `#${this.state.result.id}`;
      let time = moment(this.state.result.created_at).fromNow();
      return (
        <div>
          <h1>{this.state.result.title}</h1>

          <div>
            <span className="issues-state">{this.state.result.state}</span>
            {id} opened {time} by {this.state.result.user.login}
          </div>

          <div>
            {this.state.comments.map(comments => (
              <div className="comments-div">
                {comments.body}
                <div className="comment-text">
                  {comments.user.login} commented{" "}
                  {moment(comments.created_at).fromNow()}
                </div>
              </div>
            ) 
            // {
            //   if (comments.user.login === "jacky1205") {
            //     return (
            //       <div className="comments-div">
            //         {comments.body}
            //         <button>Delete</button>
            //         <div className="comment-text">
            //           {comments.user.login} commented{" "}
            //           {moment(comments.created_at).fromNow()}
            //         </div>
            //       </div>
            //     );
            //   }
            //   else{
            //     return (
            //       <div className="comments-div">
            //         {comments.body}
            //         <div className="comment-text">
            //           {comments.user.login} commented{" "}
            //           {moment(comments.created_at).fromNow()}
            //         </div>
            //       </div>
            //     );
            //   }
            // }
            )}
          </div>
          <label>
            <input
              class="new-comment"
              type="text"
              placeholder="add comment"
              onKeyPress={addComment}
            />
          </label>
        </div>
      );
    } else {
      return <div />;
    }
  }
}

export default SingleIssue;
