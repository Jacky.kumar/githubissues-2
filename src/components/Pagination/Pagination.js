import React from 'react';
import ReactPaginate from 'react-paginate';

const Pagination=props=>{

return(
  <ReactPaginate className="pages" previousLabel={'previous'} nextLabel={'next'} pageCount={10} onPageChange={props.handlePage} activeClassName={"active-page"} containerClassName="pagination">

  </ReactPaginate>
)


}

export default Pagination;