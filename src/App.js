import React, { Component } from "react";

import "./App.css";
import Issues from "./components/Issues";

import Header from "./components/Header";
import Pagination from "./components/Pagination/Pagination";
import {BrowserRouter,Route} from 'react-router-dom';

import Fuse from "fuse.js";

import SingleIssue from "./components/RouterLinks/SingleIssue";


class App extends Component {
  
  state = {
    result: undefined,
    HeaderStore:undefined
  };

  Labels = () => {
    return this.state.HeaderStore.reduce((labels, issues) => {
      issues.props.children.labels.forEach(label => {
        if (!labels.includes(label.name)) {
          labels.push(label.name);
        }
      });
      return labels;
    }, []);
  };

  Authors = () => {
    return this.state.HeaderStore.reduce((Author, issues) => {
      if (!Author.includes(issues.props.children.user.login)) {
        Author.push(issues.props.children.user.login);
      }
      return Author;
    }, []);
  };

  countOpenState = () => {
    let count = 0;
    this.state.HeaderStore.forEach(issues => {
      if (issues.props.children.state === "open") count++;
    });
    return count;
  };

  countClosedState = () => {
    let count = 0;
    this.state.HeaderStore.forEach(issues => {
      if (issues.props.children.state === "close") count++;
    });
    return count;
  };

  handleState = clickState => {
    this.setState({
      result: this.state.HeaderStore.filter(issues => {
        if (issues.props.children.state === clickState) return issues;
      })
    });
  };
  handleLabel = event => {
    
    this.setState({
      result: this.state.HeaderStore
        .filter(issues => {
          let avaliable = false;
          issues.props.children.labels.forEach(label => {
            if (label.name === event.target.value) {
              avaliable = true;
            }
          });
          if (avaliable) return issues;
        })
        
    });
  };
  handleAuthor = event => {
    this.setState({
      result: this.state.HeaderStore
        .filter(issues => issues.props.children.user.login === event.target.value)
      
    });
  };
  handleTitleSearch = event => {
    var options = {
      shouldSort: true,

      maxPatternLength: 10,
      minMatchCharLength: 3,
      keys: ["title"]
    };
    var fuse = new Fuse(this.state.HeaderStore, options); 
    var titleMatched = fuse.search(event.target.value);
    this.setState({
      result: titleMatched.map(issuesData => <Issues>{issuesData}</Issues>)
    });
  };

  handleSort = event => {
   
    if (event.target.value === "newest")
      this.setState({
        result: this.state.HeaderStore
          .filter(issues => issues)
          .sort(
            (issuesData1, issuesData2) =>
              new Date(issuesData2.props.children.created_at).getTime() -
              new Date(issuesData1.props.children.created_at).getTime()
          )
          
      });
    if (event.target.value === "oldest")
      this.setState({
        result: this.state.HeaderStore
          .filter(issues => issues)
          .sort(
            (issuesData1, issuesData2) =>
              new Date(issuesData1.props.children.created_at).getTime() -
              new Date(issuesData2.props.children.created_at).getTime()
          )
         
      });
    if (event.target.value === "recently updated")
      this.setState({
        result: this.state.HeaderStore
          .filter(issues => issues)
          .sort(
            (issuesData1, issuesData2) =>
              new Date(issuesData2.props.children.updated_at).getTime() -
              new Date(issuesData1.props.children.updated_at).getTime()
          )
         
      });
    if (event.target.value === "least recently updated")
      this.setState({
        result: this.state.HeaderStore
          .filter(issues => issues)
          .sort(
            (issuesData1, issuesData2) =>
              new Date(issuesData1.props.children.updated_at).getTime() -
              new Date(issuesData2.props.children.updated_at).getTime()
          )
          
      });
  };

  handlePageChange = event => {
    fetch(
      `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues?page=${event.selected +
        1}`
    )
      .then(res => res.json())
      .then(issues => {
        this.setState({
          result: issues.map(issues => <Issues>{issues}</Issues>),
          HeaderStore:issues.map(issues => <Issues>{issues}</Issues>)
        });
      });
  };

  componentDidMount() {
    fetch(
      `https://api.github.com/repos/freeCodeCamp/freeCodeCamp/issues`,
    
    )
      .then(res => res.json())
      .then(issues => {
        this.setState({
          result: issues.map(issues => <Issues>{issues}</Issues>),
          HeaderStore:issues.map(issues => <Issues>{issues}</Issues>)
        });
     
      })
      .catch(err => console.log(err));
    
  }

  render() {
    
    if(this.state.result && this.state.HeaderStore){
      
    return (
      <BrowserRouter>
      <Route path="/" component={()=>{
        return(
          <div>
          <Header
            labelData={this.Labels}
            AuthorData={this.Authors}
            onClick={this.handleState}
            onChangeLabel={this.handleLabel}
            onChangeAuthor={this.handleAuthor}
            onSort={this.handleSort}
            openIssues={this.countOpenState}
            closeIssues={this.countClosedState}
            onClickTitle={this.handleTitleSearch}
          />
          <div>{this.state.result}</div>
          <Pagination handlePage={this.handlePageChange} />
        </div>

        )
      }} exact>
     
      </Route>
      <Route path="/:number" component={SingleIssue} exact></Route>
      </BrowserRouter>
    );
    }
    else{
      return(
        <div></div>
      )
    }
  }
}

export default App;
